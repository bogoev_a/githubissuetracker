import {
  Keyboard,
  Pressable,
  PressableProps,
  StyleSheet,
  ViewStyle,
} from "react-native";
import React, { FC } from "react";

interface Props extends PressableProps {
  style?: ViewStyle;
}

const CustomKeyboardDismissContainer: FC<Props> = ({ style, ...props }) => {
  return (
    <Pressable
      style={[styles.container, style]}
      accessible={false}
      onPress={Keyboard.dismiss}
      {...props}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default CustomKeyboardDismissContainer;
