import React from "react";
import { StyleSheet } from "react-native";
import CustomTouchableOpacity from "../touchableopacity/CustomTouchableOpacity";
import BackIcon from "@assets/svg/back.svg";
import { useNavigation } from "@react-navigation/native";
import theme from "../../theme/theme";

const Back = () => {
  const navigation = useNavigation();

  return (
    <CustomTouchableOpacity
      accessibilityRole="button"
      onPress={() => navigation.goBack()}
      style={styles.back}
    >
      <BackIcon width={32} height={32} />
    </CustomTouchableOpacity>
  );
};

const styles = StyleSheet.create({
  back: {
    alignSelf: "flex-start",
    marginLeft: 14,
    marginBottom: theme.spacing.medium,
  },
});

export default Back;
