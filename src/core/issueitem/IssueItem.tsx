import React, { useEffect, useState } from "react";
import { VFC } from "react";
import { StyleSheet, Text, TouchableOpacityProps, View } from "react-native";
import OpenIssueIcon from "../../../assets/svg/open-issue.svg";
import ClosedIssueIcon from "../../../assets/svg/closed-issue.svg";
import theme from "../../theme/theme";
import CustomTouchableOpacity from "../touchableopacity/CustomTouchableOpacity";
import { IssueIntercept, WithoutChildren } from "../../types";
import Bookmark from "../bookmark/Bookmark";
import { issueSearchParamsExistInStorage } from "../bookmark/bookmarkHelper";
import {
  removeIssueSearchParamsFromStorage,
  setIssueSearchParamsInStorage,
} from "../../utils/utils";

interface Props extends WithoutChildren<TouchableOpacityProps> {
  issueIntercept: Omit<IssueIntercept, "body" | "user[avatar_url]">;
}

const IssueItem: VFC<Props> = ({ style, issueIntercept, ...props }) => {
  const { closed_at, title, number, created_at, user, owner, repo } =
    issueIntercept;

  const [isBookmarked, setIsBookmarked] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        setIsBookmarked(
          await issueSearchParamsExistInStorage({ owner, repo, number })
        );
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  const handlePress = async () => {
    try {
      const exists = await issueSearchParamsExistInStorage({
        owner,
        repo,
        number,
      });

      if (exists) {
        await removeIssueSearchParamsFromStorage({ owner, repo, number });
        setIsBookmarked(false);
      } else {
        await setIssueSearchParamsInStorage({ owner, repo, number });
        setIsBookmarked(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <CustomTouchableOpacity style={[styles.container, style]} {...props}>
      <View style={styles.topContainer}>
        {closed_at ? (
          <ClosedIssueIcon style={styles.icon} />
        ) : (
          <OpenIssueIcon style={styles.icon} />
        )}
        <View style={styles.textContainer}>
          <Text style={styles.title} numberOfLines={1}>
            {title}
          </Text>
          <Text
            numberOfLines={1}
            style={styles.description}
          >{`#${number} opened on ${new Date(created_at).toLocaleString()} by ${
            user.login
          }`}</Text>
        </View>
        <Bookmark isBookmarked={isBookmarked} onPress={handlePress} />
      </View>
    </CustomTouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: theme.spacing.small,
    backgroundColor: theme.palette.white,
    borderColor: theme.palette.neutral,
    borderWidth: 1,
    minHeight: 62,
  },
  topContainer: {
    flexDirection: "row",
  },
  icon: {
    marginHorizontal: theme.spacing.small,
  },
  textContainer: {
    flex: 1,
  },
  title: {
    ...theme.typography.largeText,
    color: theme.palette.veryDarkGrey,
  },
  description: {
    ...theme.typography.smallText,
    marginTop: theme.spacing.verySmall,
    color: theme.palette.darkGrey,
  },
});

export default IssueItem;
