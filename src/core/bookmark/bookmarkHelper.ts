import { GetIssueSearchParams } from "../../types";
import { getAllIssueSearchParamsFromStorage } from "../../utils/utils";

export const issueSearchParamsExistInStorage = async ({
  owner,
  repo,
  number,
}: GetIssueSearchParams) => {
  const issueSearchParams = await getAllIssueSearchParamsFromStorage();

  return !!issueSearchParams.find(
    (isp) =>
      isp.owner.toLowerCase() === owner.toLowerCase() &&
      isp.repo.toLowerCase() === repo.toLowerCase() &&
      isp.number == number
  );
};
