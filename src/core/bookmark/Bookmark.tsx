import React, { VFC } from "react";
import CustomTouchableOpacity from "../touchableopacity/CustomTouchableOpacity";
import BookIcon from "../../../assets/svg/book.svg";
import BookYellowIcon from "../../../assets/svg/book-yellow.svg";
import { WithoutChildren } from "../../types";
import { TouchableOpacityProps } from "react-native";

interface Props extends WithoutChildren<TouchableOpacityProps> {
  isBookmarked: boolean;
}

const Bookmark: VFC<Props> = ({ isBookmarked, ...props }) => {
  return (
    <CustomTouchableOpacity {...props}>
      {isBookmarked ? <BookYellowIcon /> : <BookIcon />}
    </CustomTouchableOpacity>
  );
};

export default Bookmark;
