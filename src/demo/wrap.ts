// Can you show us a function, wrap, that has two overloads:
// - The first one takes 1 parameter, a string, and the output is that string wrapped in parentheses.
// - The second one takes 2 parameters, a number and a string, and the output is that string wrapped in that number of parentheses.
// You should follow the template (only place code where the comments are; the parameters, the return value, and the implementation).
// You should use TypeScript & type narrowing to handle the two overloads.

export function wrap(myStr: string): string;
export function wrap(layers: number, myStr: string): string;

export function wrap(layersOrString: number | string, myStr?: string): string {
  let result;
  if (typeof layersOrString === "number") {
    const front = Array(layersOrString).fill("(").join("");
    const back = Array(layersOrString).fill(")").join("");
    result = `${front}${myStr}${back}`;
  } else {
    result = `(${layersOrString})`;
  }

  return result;
}
