import { wrap } from "./wrap";

const assertEqual = (actual: string, expected: string) => {
  if (actual !== expected) {
    throw new Error(`Expected ${expected}, got ${actual}`);
  }
};

it("hello", () => {
  // First overload
  assertEqual(wrap("hello, world!"), "(hello, world!)");
  assertEqual(wrap(""), "()");

  // Second overload
  assertEqual(wrap(6, "hello"), "((((((hello))))))");
  assertEqual(wrap(0, "hello"), "hello");
  assertEqual(wrap(1, "hello"), "(hello)");
});

// TypeScript errors. Would not necessarily throw an error, but TypeScript should complain.
// wrap("hello", undefined);
// wrap(null);
// wrap(undefined);
// wrap(6);
// wrap({});
// wrap(null, "hello");
// wrap(undefined, "hello");
// wrap(6, undefined);
// wrap("hello", null);
