import React, { useState, VFC } from "react";
import { Text, StyleSheet, ActivityIndicator, ScrollView } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import CustomKeyboardDismissContainer from "../../core/container/CustomKeyboardDismissContainer";
import Logo from "@assets/svg/logo.svg";
import BookYellowIcon from "@assets/svg/book-yellow.svg";
import theme from "../../theme/theme";
import { fetchIssuesByOwnerAndRepo } from "../../http/get";
import Pagination from "./pagination/Pagination";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import {
  BOOKMARKS_SCREEN,
  DETAILS_SCREEN,
  HOME_SCREEN,
} from "../../utils/constants";
import { RootStackParamList } from "../../router/Router";
import { FetchIssueSearchParams, IssueIntercept } from "../../types";
import CustomForm from "./form/CustomForm";
import { extractSubpages } from "./homeHelper";
import CustomTouchableOpacity from "../../core/touchableopacity/CustomTouchableOpacity";
import IssueItem from "../../core/issueitem/IssueItem";

const BATCH = 10;

const Home: VFC<
  NativeStackScreenProps<RootStackParamList, typeof HOME_SCREEN>
> = ({ navigation }) => {
  const [nextPage, setNextPage] = useState(1);
  const [issues, setIssues] = useState<IssueIntercept[]>([]);
  const [pageIndex, setPageIndex] = useState(0);
  const [numbers, setNumbers] = useState<number[]>([]);
  const [searchParams, setSearchParams] = useState<FetchIssueSearchParams>();
  const [isLoading, setIsLoading] = useState(false);

  const search = async ({ owner, repo, state }: FetchIssueSearchParams) => {
    try {
      setIsLoading(true);
      setSearchParams({ owner, repo, state });

      const currentIssues = await fetchIssuesByOwnerAndRepo({
        owner,
        repo,
        state,
        page: 1,
      });

      setIssues(currentIssues.map((c) => ({ ...c, owner, repo, state })));
      setNextPage(2);
      const nums = extractSubpages(currentIssues.length, BATCH, 0, true);
      setNumbers(nums);
      setPageIndex(0);
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  const prevOrNext = async (isNext: boolean) => {
    try {
      setIsLoading(true);
      const { owner, repo, state } = searchParams as FetchIssueSearchParams;

      const currentIssues = await fetchIssuesByOwnerAndRepo({
        owner,
        repo,
        state,
        page: isNext ? nextPage : nextPage - 2,
      });

      setIssues(currentIssues.map((c) => ({ ...c, owner, repo, state })));
      const nums = extractSubpages(
        currentIssues.length,
        BATCH,
        isNext ? numbers[numbers.length - 1] : numbers[0],
        isNext
      );
      setNumbers(nums);
      setPageIndex(0);
      setNextPage(isNext ? nextPage + 1 : nextPage - 1);
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <SafeAreaView>
      <ScrollView style={styles.content}>
        <CustomKeyboardDismissContainer>
          <CustomTouchableOpacity
            onPress={() => navigation.navigate(BOOKMARKS_SCREEN)}
            style={styles.bookmarks}
          >
            <BookYellowIcon width={32} height={32} />
          </CustomTouchableOpacity>
          <Logo style={styles.logo} />
          <Text style={styles.title}>Github Issue Tracker</Text>
          <CustomForm style={styles.marginBottom} onSubmit={search} />
          {isLoading && (
            <ActivityIndicator
              style={styles.marginTop}
              color={theme.palette.darkBlue}
            />
          )}
          {issues
            .slice(pageIndex * BATCH, (pageIndex + 1) * BATCH)
            .map((issue) => (
              <IssueItem
                key={`${issue.owner}_${issue.repo}_${issue.number}`}
                issueIntercept={issue}
                onPress={() =>
                  navigation.navigate(DETAILS_SCREEN, {
                    owner: issue.owner,
                    repo: issue.repo,
                    number: issue.number,
                  })
                }
              />
            ))}
          {numbers.length > 0 ? (
            <Pagination
              numbers={numbers}
              activeIndex={pageIndex}
              onPrev={() => prevOrNext(false)}
              onNext={() => prevOrNext(true)}
              onChangeActiveNumber={setPageIndex}
              style={styles.marginTop}
            />
          ) : null}
        </CustomKeyboardDismissContainer>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  bookmarks: {
    alignSelf: "flex-end",
    marginBottom: theme.spacing.small,
    marginTop: "10%",
  },
  content: {
    paddingHorizontal: 20,
  },
  marginBottom: {
    marginBottom: theme.spacing.large,
  },
  marginTop: {
    marginTop: theme.spacing.large,
  },
  logo: {
    alignSelf: "center",
    marginBottom: theme.spacing.large,
  },
  title: {
    ...theme.typography.verylargeText,
    color: theme.palette.veryDarkGrey,
    alignSelf: "center",
    marginBottom: theme.spacing.medium,
  },
});

export default Home;
