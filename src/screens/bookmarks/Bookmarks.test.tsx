import React from "react";
import "react-native";
import Bookmarks from "./Bookmarks";
import { act, create, ReactTestRenderer } from "react-test-renderer";
import * as utils from "../../utils/utils";
import { getIssue } from "../../http/get";
import { Issue } from "../../types";

jest.mock("@react-navigation/core");

jest.mock("../../http/get");

const mockedGetIssue = getIssue as jest.MockedFunction<typeof getIssue>;

describe("Bookmarks", () => {
  it("should render correctly", async () => {
    const data = [{ owner: "owner", repo: "repo", number: 1 }];
    jest
      .spyOn(utils, "getAllIssueSearchParamsFromStorage")
      .mockResolvedValueOnce(data);

    const issue: Issue = {
      number: 1,
      title: "title",
      user: {
        login: "login",
        avatar_url: "",
      },
      body: "body",
      created_at: "2012-04-23T18:25:43.511Z",
      closed_at: null,
    };

    mockedGetIssue.mockResolvedValueOnce(issue);

    let component: ReactTestRenderer | undefined;
    await act(async () => {
      component = await create(<Bookmarks />);
    });

    expect(component).toMatchSnapshot();
  });
});
