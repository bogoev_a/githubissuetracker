import React, { useEffect, useState, VFC } from "react";
import { RefreshControl, StyleSheet } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { getIssue } from "../../http/get";
import { getAllIssueSearchParamsFromStorage } from "../../utils/utils";
import { IssueIntercept } from "../../types";
import IssueList from "../home/issuelist/IssueList";
import Back from "../../core/back/Back";

const Bookmarks: VFC = () => {
  const [bookmarkedIssues, setBookmarkedIssues] = useState<IssueIntercept[]>(
    []
  );
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        const issueSearchParams = await getAllIssueSearchParamsFromStorage();

        const issues = await Promise.all(
          issueSearchParams.map(async (isp) => {
            const currentIssue = await getIssue(isp);
            return { ...currentIssue, ...isp };
          })
        );

        setBookmarkedIssues(issues);
      } catch (error) {
        console.log(error);
      } finally {
        setRefreshing(false);
      }
    })();
  }, [refreshing]);

  return (
    <SafeAreaView style={styles.container}>
      <Back />
      <IssueList
        data={bookmarkedIssues}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => setRefreshing(true)}
          />
        }
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1 },
});

export default Bookmarks;
