import AsyncStorage from "@react-native-async-storage/async-storage";
import { GetIssueSearchParams } from "../types";
import { ISSUE_SEARCH_PARAMS } from "./constants";

export const getAllIssueSearchParamsFromStorage = async () => {
  const issueSearchParamsJSON = await AsyncStorage.getItem(ISSUE_SEARCH_PARAMS);
  let issueSearchParams: GetIssueSearchParams[] = [];
  if (issueSearchParamsJSON) {
    issueSearchParams = JSON.parse(issueSearchParamsJSON);
  }

  return issueSearchParams;
};

export const setIssueSearchParamsInStorage = async (
  issueSearchParams: GetIssueSearchParams
) => {
  const issueSearchParamsArr = await getAllIssueSearchParamsFromStorage();
  issueSearchParamsArr.push(issueSearchParams);

  await AsyncStorage.setItem(
    ISSUE_SEARCH_PARAMS,
    JSON.stringify(issueSearchParamsArr)
  );
};

export const removeIssueSearchParamsFromStorage = async (
  issueSearchParams: GetIssueSearchParams
) => {
  const issueSearchParamsArr = await getAllIssueSearchParamsFromStorage();
  const filtered = issueSearchParamsArr.filter(
    (isp) =>
      isp.owner.toLowerCase() !== issueSearchParams.owner.toLowerCase() ||
      isp.repo.toLowerCase() !== issueSearchParams.repo.toLowerCase() ||
      isp.number != issueSearchParams.number
  );

  await AsyncStorage.setItem(ISSUE_SEARCH_PARAMS, JSON.stringify(filtered));
};
